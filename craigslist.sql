DROP DATABASE IF EXISTS CraigsList;

CREATE DATABASE IF NOT EXISTS CraigsList;
USE CraigsList;

CREATE TABLE IF NOT EXISTS USER(
id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
full_name VARCHAR(50),
age INT,
preferred_region VARCHAR(25));

CREATE TABLE IF NOT EXISTS CATEGORY(
id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
category VARCHAR(40)
);

CREATE TABLE IF NOT EXISTS REGION(
id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
region VARCHAR(30));

CREATE TABLE IF NOT EXISTS POSTS
(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
title VARCHAR(100),
text VARCHAR(250),
user_id INT,
location VARCHAR(30),
region_id INT,
category_id INT,
FOREIGN KEY (user_id) REFERENCES USER(id) ON DELETE CASCADE,
FOREIGN KEY(region_id) REFERENCES REGION(id)ON DELETE CASCADE,
FOREIGN KEY(category_id) REFERENCES CATEGORY(id) ON DELETE CASCADE);


INSERT INTO USER (full_name,age,preferred_region)
    VALUES 
        ('Mahesh Srisailam',25,'SanFrancisco'),
        ('Nagendra Putta',25,'Atlanta'),
        ('Tejas Kale',25,'Seattle');


INSERT INTO REGION(region)
    VALUES
        ('SanFrancisco'),
        ('Atlanta'),
        ('Seattle');

INSERT INTO CATEGORY(category)
    VALUES
        ('Jobs'),
        ('Services'),
        ('Sales');

INSERT INTO POSTS(title,text,user_id,location,region_id,category_id)
    VALUES
        ('Software Developer','hello,we have openings on software developer',1,'oakland',1,1),
        ('Home Maintenance & Renovation','hello,we are providing services for home maintainance & renovation',2,'midtown',2,2),
        ('Dust & SPM monitor','hello,we are providing top-quality dust control systems',3,'Fremont',3,3);



