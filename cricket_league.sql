DROP DATABASE IF EXISTS IPL;

CREATE DATABASE IPL;

USE IPL ;

CREATE TABLE IF NOT EXISTS TEAMS(
   id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
   name VARCHAR(100)
);

CREATE TABLE CATEGORY(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    category VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS PLAYERS(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    jersey_number INT,
    category_id INT,
    team_id INT,
    FOREIGN KEY (category_id) REFERENCES CATEGORY(id) ON DELETE CASCADE,
    FOREIGN KEY (team_id) REFERENCES TEAMS(id) ON DELETE CASCADE

);

CREATE TABLE IF NOT EXISTS UMPIRES(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    name VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS MATCHES(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    team1_id INT NOT NULL,
    team2_id INT NOT NULL,
    team1_score INT,
    team2_score INT,
    win_team_id INT,
    win_text TEXT,
    umpire_id INT,
    venue TEXT,
    date DATETIME,
    FOREIGN KEY (team1_id) REFERENCES TEAMS(id) ON DELETE CASCADE,
    FOREIGN KEY (team2_id) REFERENCES TEAMS(id) ON DELETE CASCADE,
    FOREIGN KEY (win_team_id) REFERENCES TEAMS(id) ON DELETE CASCADE,
    FOREIGN KEY (umpire_id) REFERENCES UMPIRES(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS PLAYER_STATS(
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    player_id INT NOT NULL,
    score INT,
    wickets INT,
    match_id INT NOT NULL,
    FOREIGN KEY (player_id) REFERENCES PLAYERS(id) ON DELETE CASCADE,
    FOREIGN KEY (match_id) REFERENCES MATCHES(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS SEASONS (
  s_no INT NOT NULL,
  s_year INT NOT NULL,
  start_date DATETIME,
  end_date DATETIME,
  matches INT NOT NULL,
  sponsor VARCHAR(20) NOT NULL,
  runs INT NOT NULL,
  wickets INT NOT NULL,
  catches INT NOT NULL,
  fours INT NOT NULL,
  sixes INT NOT NULL,
  winteam_id INT NOT NULL,
  mvp_id INT NOT NULL,
  FOREIGN KEY (winteam_id) REFERENCES TEAMS(id) ON DELETE CASCADE,
  FOREIGN KEY (mvp_id) REFERENCES PLAYERS(id) ON DELETE CASCADE
);


CREATE TABLE IF NOT EXISTS STANDINGS(
   rank_no INT PRIMARY KEY NOT NULL,
   team_id INT NOT NULL,
   played INT NOT NULL,
   won INT,
   lost INT,
   tied INT,
   net_runrate FLOAT ,
   points INT,
   FOREIGN KEY (team_id) REFERENCES TEAMS(id) ON DELETE CASCADE
);


INSERT INTO TEAMS(name)
    VALUES
        ('Sun Risers Hyderabad'),
        ("Chennai Super Kings"),
        ('Mumbai Indians'),
        ('Rajasthan Royals'),
        ('Kolkatha Knight Riders'),
        ('Royal Challengers Bangalore'),
        ('Delhi Capitals'),
        ('Punjab Kings'),
        ('Gujarat Titans'),
        ('Lucknow Super Giants');

INSERT INTO CATEGORY(category)
    VALUES
        ('Batsman'),
        ('Bowler'),
        ('Wicket-Keeper'),
        ('All-Rounder');




INSERT INTO UMPIRES(name)
    VALUES
        ('Nitin Meneon'),
        ('Kumar Dharmasena'),
        ('Anil Choudhary'),
        ('Chettithody Shamshuddin');


INSERT INTO PLAYERS(name,jersey_number,category_id,team_id)
    VALUES
        ('Ms Dhoni',7,3,2),
        ('Rohit Sharama',45,1,3),
        ('Kane Williamson',31,1,1),
        ('Virat Kohli',18,1,6),
        ('Hardik Pandya',33,4,9);

INSERT INTO MATCHES(team1_id,team2_id,team1_score,team2_score,win_team_id,win_text,umpire_id,venue,date)
    VALUES
        (1,2,180,170,1,'Won by 10 Runs',1,'Hyderabad','2021-04-25'),
        (3,4,190,191,3,'Won by 5 Wickets',4,'Rajasthan','2021-04-28');

INSERT INTO PLAYER_STATS(player_id,score,wickets,match_id)
    VALUES
        (5,61,2,2),
        (1,69,0,1);

INSERT INTO SEASONS(s_no,s_year,start_date,end_date, matches, sponsor, runs, wickets, catches, fours, sixes,winteam_id,mvp_id) 
    VALUES
    (8, 2016,'2016-04-12','2016-05-29', 60, 'Vivo IPL', 10455, 485, 360, 2541, 660, 1, 5);


INSERT INTO STANDINGS(rank_no,team_id,played,won,lost,tied,net_runrate,points)
    VALUES
        (1,9,14,10,4,0,0.316,20);
