drop database if exists MedicalCenter;

create database MedicalCenter;

use MedicalCenter;

CREATE TABLE IF NOT EXISTS DOCTOR
(id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
full_name VARCHAR(50),
field_of_specialization VARCHAR(150));  

CREATE TABLE IF NOT EXISTS PATIENT(
id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
full_name VARCHAR(25),
gender VARCHAR(10),
age INT);

CREATE TABLE IF NOT EXISTS DIAGONSIS(
id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
category VARCHAR(100));

CREATE TABLE IF NOT EXISTS VISIT(
id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
visit_date VARCHAR(30),
diagonsis_id INT,
patient_id INT,
doctor_id INT,
FOREIGN KEY (diagonsis_id) REFERENCES DIAGONSIS(id) ON DELETE CASCADE,
FOREIGN KEY (patient_id) REFERENCES PATIENT(id) ON DELETE CASCADE,
FOREIGN KEY (doctor_id) REFERENCES DOCTOR(id) ON DELETE CASCADE);

INSERT INTO DOCTOR
   (full_name,field_of_specialization)
VALUES
    ('Z S Meharwal', 'Cardiac Surgeon'),
  ('Sandeep Vaishya', 'Neurosurgeon');


INSERT INTO PATIENT (full_name,gender,age)
    VALUES 
        ('Mahesh Srisailam',"Male",25),
        ('Nagendra Putta','Male',25);


INSERT INTO DIAGONSIS (category)
    VALUES 
    ('Brain Tumor'),
    ('Heartburn');

INSERT INTO VISIT
(visit_date,diagonsis_id,patient_id,doctor_id)
VALUES
('2022-11-10',1,1,1),
('2022-10-29',2,2,2);